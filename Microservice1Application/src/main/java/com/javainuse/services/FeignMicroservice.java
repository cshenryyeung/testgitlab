package com.javainuse.services;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name="microservice2")
public interface FeignMicroservice {
	@RequestMapping(method=RequestMethod.GET, value="/microservice2")
	public String getData();

}
