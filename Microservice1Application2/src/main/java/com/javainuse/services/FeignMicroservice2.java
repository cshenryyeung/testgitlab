package com.javainuse.services;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name="microservice3")
public interface FeignMicroservice2 {
	@RequestMapping(method=RequestMethod.GET, value="/microservice3")
	public String getData();

}
